# Common WP

![](./assets/images/github.jpg)

This is a wordpress theme for non-profit use. It's focus is on simple visual design, and clean componentized code for easy customization by organizations with diverse needs.

## Requirements

In order to use this theme fully you need to install the [[Common WP Blocks]](https://github.com/jcklpe/common-wp-blocks) plugin and the [[The Events Calendar]](https://wordpress.org/plugins/the-events-calendar/). In order to make edits to the theme you need to have a basic familiarity with [cli](https://www.youtube.com/watch?v=4RPtJ9UyHS0&t=), [npm](https://www.npmjs.com/), and [gulp](https://gulpjs.com/),

## Build Process

This theme uses a gulp build system.

Explanation of commands

`npm install`: installs all the [npm](https://www.npmjs.com/) packages for running the gulp build system.

`gulp`: runs a single JS/[SCSS](https://en.wikipedia.org/wiki/Sass_\(stylesheet_language\)) build.

`gulp scss`: compiles all scss files into css

`gulp site-js`: transpiles/concatenates/minifies all theme js files

`gulp foundation-js`: transpiles/concatenates/minifies all js files used by the vendor project, foundation framework

`gulp watch`: watch the `src` folder for any changes to the files as you work, and whenever you save it will auto-build those new files.

`gulp dev`: watches files and also runs [browsersync](https://medium.com/oceanize-geeks/browsersync-for-faster-development-f27b09b9896e). Browsersync will run a proxy server that autoreloads the page whenever it builds new versions of the files. To get browsersync to work you will need to put your local dev environment url in the variable `localDevURL` in `gulpfile.js`.

## de facto components

Eventually I would like to pull all structural content patterns into custom Gutenberg block components using the [common-wp-blocks plugin](https://github.com/jcklpe/common-wp-blocks) but for the purposes of speed I've instead established some CSS class naming conventions for producing the same basic effect.

###### Resource Page Header Block

vanilla block: H2 heading

class name: section-header

function: provides a header that spans farther out than the content, helping to divide up the page into sections. 

![](./assets/images/components/section-header.png)



## TODO:

- [x] componentize out scss parts and reorganize build process
- [x] replace all hardcoded logos with global custom fields
- [x] replace all hardcoded text with global custom fields
- [x] convert all custom template blocks into template partials
- [x] white label all organizational references
- [x] break out block components into a separate companion plugin
- [ ] complete [blocks companion plugin](https://github.com/jcklpe/common-wp-blocks).
- [ ] re-align php templates in light of more flexible blocks based page building project.
- [ ] Build theme scss to "overlay" the default styling of the block companion plugin to make this easier for people to customize for other contexts
- [x] convert build process to gulp 4
- [ ] Lots of other stuff

## License

licensed under Cooperative Work License v 1.0
